package com.rephlexions.taskscheduler.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.rephlexions.taskscheduler.R;

public class AddTaskActivity extends AppCompatActivity {

    EditText titleText,contentText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        titleText = findViewById(R.id.editText);
        contentText = findViewById(R.id.editText2);

        Intent intent = getIntent();
        final int noteId = intent.getIntExtra("noteId",-1);
        if(noteId!=-1){
            titleText.setText(CalendarActivity.notes.get(noteId));
        }

        contentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CalendarActivity.notes.set(noteId,String.valueOf(s));
                CalendarActivity.arrayAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
}
