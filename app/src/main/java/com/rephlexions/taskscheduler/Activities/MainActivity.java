package com.rephlexions.taskscheduler.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rephlexions.taskscheduler.Adapter.SlideAdapter;
import com.rephlexions.taskscheduler.AuthAndRegister.LoginActivity;
import com.rephlexions.taskscheduler.R;


public class MainActivity extends AppCompatActivity {

    private ViewPager mSlideViewPager;
    private LinearLayout mDotsLayout;
    private SlideAdapter slideAdapter;
    private TextView[] mDots;
    private int mcurrentPage;
    private TextView finish;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        SharedPreferences preferences = getSharedPreferences("PREFERENCE",MODE_PRIVATE);
        String FirstTime =  preferences.getString("FirstTimeInstall","");

        if(FirstTime.equals("Yes")){

            Intent signin_signupActivity = new Intent(getApplicationContext(), signin_signupActivity.class);
            startActivity(signin_signupActivity);
            finish();

        }else{
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("FirstTimeInstall","Yes");
            editor.apply();
        }






        mSlideViewPager = findViewById(R.id.SlideViewPager);
        mDotsLayout = findViewById(R.id.dotsLayout);

        finish =  findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signin_signupActivity = new Intent(getApplicationContext(), signin_signupActivity.class);
                startActivity(signin_signupActivity);
                finish();
            }
        });

        slideAdapter =  new SlideAdapter(this);
        mSlideViewPager.setAdapter(slideAdapter);
        addDotsIndicator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);

    }


    public void addDotsIndicator(int position){
        mDots =  new TextView[3];
        mDotsLayout.removeAllViews();
        for(int i = 0; i<mDots.length;i++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorWhite));
            mDotsLayout.addView(mDots[i]);
        }
        if(mDots.length > 0){
            mDots[position].setTextColor(getResources().getColor(R.color.mainYellow));
        }
    }

    final ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
            mcurrentPage = position;
            if(position == mDots.length-1){

                finish.setText("Skip Tutorial>");

            }if(position == 2){
                finish.setText("Finish");
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
