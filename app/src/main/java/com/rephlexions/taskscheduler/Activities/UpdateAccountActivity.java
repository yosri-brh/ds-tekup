package com.rephlexions.taskscheduler.Activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rephlexions.taskscheduler.R;
import com.rephlexions.taskscheduler.Class.User;

import java.util.Calendar;

public class UpdateAccountActivity extends AppCompatActivity {

    private static final String TAG = "UpdateAccountActivity";

    ImageView ImgUserPhoto;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private EditText userEmail,userName;
    private EditText ownerAdress,petName,petInfo,DeviceID;
    private ProgressBar loadingProgress;
    private Button regBtn,regBtnlgn;
    private TextView mDisplayDate,exit;
    DatabaseReference reference;
    private RadioGroup Gender;
    private FirebaseAuth mAuth;
    AppCompatRadioButton Male, Female;
    String uid;
    FirebaseUser User;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_account);
        User = FirebaseAuth.getInstance().getCurrentUser();
        uid = User.getUid();

        DeviceID = findViewById(R.id.PdeviceID);
        userEmail = findViewById(R.id.login_mail);
        Gender = (RadioGroup) findViewById(R.id.Pgender);
        userName = findViewById(R.id.UserName);
        ownerAdress = findViewById(R.id.Oadr);
        petInfo = findViewById(R.id.Pinfo);
        petName = findViewById(R.id.namePet);
        regBtn = findViewById(R.id.regBtn);
        regBtnlgn = findViewById(R.id.regBtnlgn);
        exit = findViewById(R.id.exit);
        loadingProgress = findViewById(R.id.regProgressBar);
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
        mAuth = FirebaseAuth.getInstance();
        loadingProgress.setVisibility(View.INVISIBLE);

        mDisplayDate = (TextView) findViewById(R.id.Datepet);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent accountActivity = new Intent(getApplicationContext(), AccountActivity.class);
                startActivity(accountActivity);
                finish();
            }
        });

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        UpdateAccountActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;

                Log.d(TAG, "onDateSet: dd/mm/yyyy " + day + "/" + month + "/" + year);

                String date = day + "/" + month + "/" + year;
                mDisplayDate.setText(date);




            }
        };

        reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String pName = dataSnapshot.child(uid).child("petname").getValue(String.class);
                String pDate = dataSnapshot.child(uid).child("date").getValue(String.class);
                String petinfo = dataSnapshot.child(uid).child("petinfo").getValue(String.class);
                String Adress = dataSnapshot.child(uid).child("owner").getValue(String.class);
                String Gender = dataSnapshot.child(uid).child("gender").getValue(String.class);
                String Email = dataSnapshot.child(uid).child("email").getValue(String.class);
                String user = dataSnapshot.child(uid).child("name").getValue(String.class);
                String deviceid = dataSnapshot.child(uid).child("deviceid").getValue(String.class);




                petName.setText(pName);
                mDisplayDate.setText(pDate);
                userName.setText(user);
                ownerAdress.setText(Adress);
                petInfo.setText(petinfo);
                userEmail.setText(Email);
                DeviceID.setText(deviceid);
                UpdateProfilPic();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getApplicationContext(),
                        "Network ERROR , please check your connection ",Toast.LENGTH_LONG).show();

            }
        });

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                regBtn.setVisibility(View.INVISIBLE);
                loadingProgress.setVisibility(View.VISIBLE);
                final String date = mDisplayDate.getText().toString();
                final String email = userEmail.getText().toString();
                final String name = userName.getText().toString();
                final String gender = ((RadioButton)findViewById(Gender.getCheckedRadioButtonId())).getText().toString();
                final String owner = ownerAdress.getText().toString();
                final String petinfo = petInfo.getText().toString();
                final String petname = petName.getText().toString();
                final String deviceid = DeviceID.getText().toString();


                //control Saisie

                    if( email.isEmpty() || name.isEmpty()||owner.isEmpty()
                            || petinfo.isEmpty() || petname.isEmpty() || date.matches("Select Date")
                            || gender.isEmpty() || deviceid.isEmpty()){
                        showMessage("Please fill All fields Properly Bro !");
                        regBtn.setVisibility(View.VISIBLE);
                        loadingProgress.setVisibility(View.INVISIBLE);
                    }else
                    {

                        UpdateUserAccount(email,petname,owner,date,petinfo,gender,deviceid,name);
                    }





            }
        });




        //storage stuff
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
        //UI Stuff
        Male = findViewById(R.id.Male);
        Female = findViewById(R.id.Female);
        //Storage image Permission
        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT >= 22) {

                    checkAndRequestForPermission();
                }
                else{

                    openGallery();
                }
            }

        });
    }
    private void UpdateUserAccount(final String email, final String petname,
                                   final String owner, final String date,
                                   final String petinfo, final String gender,final String deviceid, final String name) {

        User user = new User(petname,email,name, owner, date, petinfo,gender,deviceid);
        FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){
                    showMessage("Profile Updated Successfully" );
                    finish();
                    regBtn.setVisibility(View.VISIBLE);
                    loadingProgress.setVisibility(View.INVISIBLE);
                }
            }
        });

    }


    private void updateUI() {

        Intent accountActivity = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(accountActivity);
        finish();



    }

    private void openGallery() {

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,REQUESCODE);
    }


    private void checkAndRequestForPermission()
    {


        if(ContextCompat.checkSelfPermission(UpdateAccountActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {

            if(ActivityCompat.shouldShowRequestPermissionRationale(UpdateAccountActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                Toast.makeText(UpdateAccountActivity.this,"Please accept the required Permission",Toast.LENGTH_SHORT).show();
            }
            else
            {
                ActivityCompat.requestPermissions(UpdateAccountActivity.this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE},PReqCode);
            }
        }
        else
            openGallery();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == REQUESCODE && data != null){
            //user jawou behi ikhtar el taswira  finalement w hamdoulah
            // taswira fel URI

            pickedImgUri = data.getData();
            ImgUserPhoto.setImageURI(pickedImgUri);

        }
    }

    public void OnRadioButtonClicked (View view) {

        boolean isSelected = ((AppCompatRadioButton) view).isChecked();

        switch (view.getId()){

            case R.id.Male:

                if(isSelected){

                    Male.setTextColor(Color.WHITE);
                    Female.setTextColor(Color.GRAY);
                }

                break;

            case R.id.Female:
                if(isSelected){

                    Female.setTextColor(Color.WHITE);
                    Male.setTextColor(Color.GRAY);
                }

                break;
        }
    }
    private void showMessage(String message) {


        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();



    }
    private void UpdateProfilPic() {


        Glide.with(getApplicationContext()).load(User.getPhotoUrl()).centerCrop().into(ImgUserPhoto);


    }
}
