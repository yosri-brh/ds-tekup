package com.rephlexions.taskscheduler.Class;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.rephlexions.taskscheduler.Activities.MapActivity;
import com.rephlexions.taskscheduler.R;

import static com.rephlexions.taskscheduler.Class.App.CHANNEL_ID;

public class ServiceBg extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String d = intent.getStringExtra("inputExtra");
        double distance = Double.parseDouble(d);
        Intent notificationIntent = new Intent(this, MapActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        if(distance>1)
        {




            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Your Pet Is far By 1km now!")
                    .setContentText("it's far by "+d+" km start tracking")
                    .setSmallIcon(R.drawable.mainlogo)
                    .setContentIntent(pendingIntent)
                    .build();
            startForeground(1, notification);


        }
        //do heavy work on a background thread
        //stopSelf();
        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
