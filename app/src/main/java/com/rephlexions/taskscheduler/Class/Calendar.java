package com.rephlexions.taskscheduler.Class;

public class Calendar {
    private String Title, Description, Date, Time;

    public Calendar(String title, String description, String date, String time) {
        Title = title;
        Description = description;
        Date = date;
        Time = time;
    }

    public Calendar() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
