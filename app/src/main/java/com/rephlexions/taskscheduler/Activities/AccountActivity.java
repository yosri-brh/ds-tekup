package com.rephlexions.taskscheduler.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rephlexions.taskscheduler.MainActivity;
import com.rephlexions.taskscheduler.R;

public class AccountActivity extends AppCompatActivity {

    TextView petname,date,username,adress,info,gal;
    String uid;
    FirebaseUser User;
    DatabaseReference reference,refPick;
    ImageView PhotoProfil,g1,g2,Setting,Edit_Profile;
    Button addPic;
    private TextView mTextViewShowUploads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        User = FirebaseAuth.getInstance().getCurrentUser();
        uid = User.getUid();
        Edit_Profile = findViewById(R.id.Edit_Profile);
        Setting = findViewById(R.id.Setting);
        addPic = findViewById(R.id.addPic);
        g1 = findViewById(R.id.G1);
        g2 = findViewById(R.id.G2);
        PhotoProfil = findViewById(R.id.profilPic);
        petname = findViewById(R.id.userpetName);
        date = findViewById(R.id.petDate);
        username = findViewById(R.id.userName);
        adress = findViewById(R.id.userAdress);
        info = findViewById(R.id.petInfo);
        gal = findViewById(R.id.textView10);
        mTextViewShowUploads = findViewById(R.id.text_tiew_show_uploads);

        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImagesActivity();
            }


        });

        Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent setting_Activity = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(setting_Activity);
                finish();

            }
        });


        addPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //myDialog.setContentView(R.layout.upload_gallery);
                //myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                //myDialog.show();
                Intent upload_Activity = new Intent(getApplicationContext(), Upload_Gallery.class);
                startActivity(upload_Activity);
                finish();


            }
        });
        Edit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent updateaccountActivity = new Intent(getApplicationContext(), UpdateAccountActivity.class);
                startActivity(updateaccountActivity);
                finish();

            }
        });


        reference = FirebaseDatabase.getInstance().getReference("Users");
        refPick = FirebaseDatabase.getInstance().getReference("uploads").child(uid);
        reference.keepSynced(true);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String pName = dataSnapshot.child(uid).child("petname").getValue(String.class);
                String pDate = dataSnapshot.child(uid).child("date").getValue(String.class);
                String UserName = dataSnapshot.child(uid).child("name").getValue(String.class);
                String petInfo = dataSnapshot.child(uid).child("petinfo").getValue(String.class);
                String Adress = dataSnapshot.child(uid).child("owner").getValue(String.class);
                String Gender = dataSnapshot.child(uid).child("gender").getValue(String.class);




                petname.setText(pName);
                date.setText("next Birthday " + pDate + ":3");
                username.setText(UserName);
                adress.setText(Adress);
                info.setText(petInfo);
                gal.setText(pName +"'s Gallery");

                UpdateProfilPic();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getApplicationContext(),
                        "Network ERROR , please check your connection Bro!",Toast.LENGTH_LONG).show();

            }
        });




        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNav);

        bottomNavigationView.setSelectedItemId(R.id.account);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.mapp:
                        startActivity(new Intent(getApplicationContext(), MapActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.veterinary:
                        startActivity(new Intent(getApplicationContext(), VeterinaryActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.calender:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }

                return false;
            }
        });

    }
    private void openImagesActivity() {
        Intent imagesActivity = new Intent(getApplicationContext(), ImagesActivity.class);
        startActivity(imagesActivity);
        finish();
    }

    private void UpdateProfilPic() {

        final ImageView g1 = findViewById(R.id.G1);
        final ImageView g2 = findViewById(R.id.G2);

        ImageView photoprofil = findViewById(R.id.profilPic);
        Glide.with(this).load(User.getPhotoUrl()).centerCrop().into(photoprofil);
        Glide.with(this).load(User.getPhotoUrl()).centerCrop().into(g1);
        Glide.with(this).load(User.getPhotoUrl()).centerCrop().into(g2);
    }

}
