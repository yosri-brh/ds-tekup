package com.rephlexions.taskscheduler.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.rephlexions.taskscheduler.R;

public class LanguageActivity extends AppCompatActivity {

    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setting_Activity = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(setting_Activity);
                finish();
            }

        });
    }
}
