package com.rephlexions.taskscheduler.AuthAndRegister;
import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.rephlexions.taskscheduler.Activities.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.rephlexions.taskscheduler.Class.CaptureAct;
import com.rephlexions.taskscheduler.Class.User;
import com.rephlexions.taskscheduler.R;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "RegisterActivity";

    ImageView ImgUserPhoto,QrCodeScanner;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private EditText userEmail,userPassword,userPassword2,userName;
    private EditText ownerAdress,petName,petInfo,DeviceID;
    private ProgressBar loadingProgress;
    private Button regBtn,regBtnlgn;
    private TextView mDisplayDate;
    private RadioGroup Gender;
    private FirebaseAuth mAuth;
 AppCompatRadioButton Male, Female;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mDisplayDate = (TextView) findViewById(R.id.Datepet);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        RegisterActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;

                Log.d(TAG, "onDateSet: dd/mm/yyyy " + day + "/" + month + "/" + year);

                String date = day + "/" + month + "/" + year;
                mDisplayDate.setText(date);




            }
        };


        //declaration FireBase stuff
        QrCodeScanner = findViewById(R.id.QrCodeScanner);
        DeviceID = findViewById(R.id.PdeviceID);
        userEmail = findViewById(R.id.login_mail);
        userPassword = findViewById(R.id.pass);
        userPassword2 = findViewById(R.id.pass2);
        Gender = (RadioGroup) findViewById(R.id.Pgender);
        userName = findViewById(R.id.UserName);
        ownerAdress = findViewById(R.id.Oadr);
        petInfo = findViewById(R.id.Pinfo);
        petName = findViewById(R.id.namePet);
        regBtn = findViewById(R.id.regBtn);
        regBtnlgn = findViewById(R.id.regBtnlgn);
        loadingProgress = findViewById(R.id.regProgressBar);
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
        mAuth = FirebaseAuth.getInstance();
        loadingProgress.setVisibility(View.INVISIBLE);


        QrCodeScanner.setOnClickListener(this);



        ////////////////////////////////////////////////////////////////


        regBtnlgn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginActivity);
                finish();

            }
        });



        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                regBtn.setVisibility(View.INVISIBLE);
                loadingProgress.setVisibility(View.VISIBLE);
                final String date = mDisplayDate.getText().toString();
                final String email = userEmail.getText().toString();
                final String password = userPassword.getText().toString();
                final String password2 = userPassword2.getText().toString();
                final String name = userName.getText().toString();
                final String gender = ((RadioButton)findViewById(Gender.getCheckedRadioButtonId())).getText().toString();
                final String owner = ownerAdress.getText().toString();
                final String petinfo = petInfo.getText().toString();
                final String petname = petName.getText().toString();
                final String deviceid = DeviceID.getText().toString();


                //control Saisie
                try{
                    if( email.isEmpty() || name.isEmpty()|| password.isEmpty() || !password.equals(password2)|| owner.isEmpty()

                            || petinfo.isEmpty() || petname.isEmpty() || date.matches("Select Date")
                            || gender.isEmpty() || password.length() < 6 || pickedImgUri == null ) {
                        showMessage("Please fill All fields Properly ");
                        regBtn.setVisibility(View.VISIBLE);
                        loadingProgress.setVisibility(View.INVISIBLE);
                    }else if((deviceid.isEmpty())||!deviceid.matches("id123|id147|id009")){

                        showMessage("pls Scan a valid QR code or type the code on the device Correctly");
                        regBtn.setVisibility(View.VISIBLE);
                        loadingProgress.setVisibility(View.INVISIBLE);
                    }
                    else
                    {
                        CreateUserAccount(email,password,petname,owner,date,petinfo,gender,deviceid,name);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        });


        //storage stuff
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
        //UI Stuff
        Male = findViewById(R.id.Male);
        Female = findViewById(R.id.Female);
        //Storage image Permission
        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT >= 22) {

                    checkAndRequestForPermission();
                }
                else{

                    openGallery();
                }
            }

        });
    }

    private void CreateUserAccount(final String email, String password, final String petname,
                                   final String owner, final String date,
                                   final String petinfo, final String gender,final String deviceid, final String name) {


        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){

                    //el compt tgad w jaw hafeli

                    showMessage("account created :D");

                    UpdateUserInfo(pickedImgUri,mAuth.getCurrentUser());  //this where we gonna add the custom fields

                    User user = new User(petname,email,name, owner, date, petinfo,gender,deviceid);
                    FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){
                                UpdateUserInfo(pickedImgUri,mAuth.getCurrentUser());

                            }

                        }
                    });

                }
                else{

                    showMessage("account creation failed " + task.getException().getMessage());
                    regBtn.setVisibility(View.VISIBLE);
                    loadingProgress.setVisibility(View.INVISIBLE);
                }



            }
        });




    }

    private void UpdateUserInfo(Uri pickedImgUri, final FirebaseUser currentUser) {

        StorageReference mStorage = FirebaseStorage.getInstance().getReference("users_photos").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        final StorageReference imageFilePath = mStorage.child(pickedImgUri.getLastPathSegment());
        imageFilePath.putFile(pickedImgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        UserProfileChangeRequest profilUpdate = new UserProfileChangeRequest.Builder()
                                .setPhotoUri(uri)
                                .build();


                        currentUser.updateProfile(profilUpdate)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful())
                                        {
//                                            showMessage("Register completed ");
                                            updateUI();
                                        }
                                    }
                                });
                    }
                });
            }
        });


    }

    private void updateUI() {

        Intent homeActivity = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(homeActivity);
        finish();



    }

    // simple display error msg
    private void showMessage(String message) {


        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();



    }

    private void openGallery() {

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,REQUESCODE);
    }


    private void checkAndRequestForPermission()
    {


        if(ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {

            if(ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                Toast.makeText(RegisterActivity.this,"Please accept the required Permission",Toast.LENGTH_SHORT).show();
            }
            else
            {
                ActivityCompat.requestPermissions(RegisterActivity.this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE},PReqCode);
            }
        }
        else
            openGallery();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == REQUESCODE && data != null){
            //user jawou behi ikhtar el taswira  finalement w hamdoulah
            // taswira fel URI

            pickedImgUri = data.getData();
            ImgUserPhoto.setImageURI(pickedImgUri);
        }
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null){
            if(result.getContents() != null){
                DeviceID.setText(result.getContents());
            }else{
                Toast.makeText(this, "Code invalid", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void OnRadioButtonClicked (View view) {

        boolean isSelected = ((AppCompatRadioButton) view).isChecked();

        switch (view.getId()){

            case R.id.Male:

                if(isSelected){

                    Male.setTextColor(Color.WHITE);
                    Female.setTextColor(Color.GRAY);
                }

                break;

            case R.id.Female:
                if(isSelected){

                    Female.setTextColor(Color.WHITE);
                    Male.setTextColor(Color.GRAY);
                }

                break;
        }
    }

    @Override
    public void onClick(View v) {

        scanCode();

    }
    private void scanCode(){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Your pet device Code");
        integrator.initiateScan();
    }

}

