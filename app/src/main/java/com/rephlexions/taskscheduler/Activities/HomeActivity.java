package com.rephlexions.taskscheduler.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rephlexions.taskscheduler.MainActivity;
import com.rephlexions.taskscheduler.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class HomeActivity extends AppCompatActivity {

//    ImageView tips_active,tips_inactive,news_active,news_inactive,blog_active,blog_inactive;
    ImageView notif_bell,articl1_image,articl2_image,articl3_image,articl4_image;
    TextView articl1_title,articl2_title,articl3_title,articl4_title,name,date_art1,date_art2,date_art3,date_art4;
    DatabaseReference reference;
    String uid;
    FirebaseUser User;
    private GoogleMap mMap;
    Location myLocation;
    DatabaseReference loca;
    DatabaseReference Trackreference;
    protected LatLng start=null;
    protected LatLng end=null;
    double distance,km;
    boolean locationPermission=false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if(ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {

        }else{
            locationPermission=true;
            ActivityCompat.requestPermissions(HomeActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},44);
        }
        User = FirebaseAuth.getInstance().getCurrentUser();
        uid = User.getUid();


        name = findViewById(R.id.textView42);
//        tips_inactive = findViewById(R.id.tips_inactive);
//        blog_active = findViewById(R.id.blog_active);
//        blog_inactive = findViewById(R.id.blog_inactive);
//        news_active =  findViewById(R.id.news_active);
//        news_inactive = findViewById(R.id.news_inactive);
        notif_bell = findViewById(R.id.notif_bell);
        articl1_title = findViewById(R.id.articl1_title);
        articl2_title = findViewById(R.id.articl2_title);
        articl3_title = findViewById(R.id.articl3_title);
        articl4_title = findViewById(R.id.articl4_title);
        articl1_image = findViewById(R.id.articl1_image);
        articl2_image = findViewById(R.id.articl2_image);
        articl3_image = findViewById(R.id.articl3_image);
        articl4_image = findViewById(R.id.articl4_image);
        date_art1 = findViewById(R.id.date_art1);
        date_art2 = findViewById(R.id.date_art2);
        date_art3 = findViewById(R.id.date_art3);
        date_art4 = findViewById(R.id.date_art4);
        notif_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent notification_Activity = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(notification_Activity);
                finish();
            }
        });

        reference = FirebaseDatabase.getInstance().getReference("Article");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String title1 = dataSnapshot.child("art1").child("title").getValue(String.class);
                        String title2 = dataSnapshot.child("art2").child("title").getValue(String.class);
                        String title3 = dataSnapshot.child("art3").child("title").getValue(String.class);
                        String title4 = dataSnapshot.child("art4").child("title").getValue(String.class);
                        String img1 = dataSnapshot.child("art1").child("imgUrl").getValue(String.class);
                        String img2 = dataSnapshot.child("art2").child("imgUrl").getValue(String.class);
                        String img3 = dataSnapshot.child("art3").child("imgUrl").getValue(String.class);
                        String img4 = dataSnapshot.child("art4").child("imgUrl").getValue(String.class);
                        final String UrlArt1 = dataSnapshot.child("art1").child("artUrl").getValue(String.class);
                        final String UrlArt2 = dataSnapshot.child("art2").child("artUrl").getValue(String.class);
                        final String UrlArt3 = dataSnapshot.child("art3").child("artUrl").getValue(String.class);
                        final String UrlArt4 = dataSnapshot.child("art4").child("artUrl").getValue(String.class);
                        String Date_art1 = dataSnapshot.child("art1").child("date").getValue(String.class);
                        String Date_art2 = dataSnapshot.child("art2").child("date").getValue(String.class);
                        String Date_art3 = dataSnapshot.child("art3").child("date").getValue(String.class);
                        String Date_art4 = dataSnapshot.child("art4").child("date").getValue(String.class);


                        Picasso.get().load(img1).fit().centerCrop().into(articl1_image);
                        Picasso.get().load(img2).fit().centerCrop().into(articl2_image);
                        Picasso.get().load(img3).fit().centerCrop().into(articl3_image);
                        Picasso.get().load(img4).fit().centerCrop().into(articl4_image);


                        articl1_title.setText(title1);
                        articl2_title.setText(title2);
                        articl3_title.setText(title3);
                        articl4_title.setText(title4);
                        date_art1.setText(Date_art1);
                        date_art2.setText(Date_art2);
                        date_art3.setText(Date_art3);
                        date_art4.setText(Date_art4);
                articl1_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(UrlArt1));
                        startActivity(intent);


                    }
                });
                articl2_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(UrlArt2));
                        startActivity(intent);

                    }
                });
                articl3_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(UrlArt3));
                        startActivity(intent);

                    }
                });
                articl4_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(UrlArt4));
                        startActivity(intent);

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String pName = dataSnapshot.child(uid).child("petname").getValue(String.class);
                name.setText(pName+" !");




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(getApplicationContext(),
                        "Network ERROR , please check your connection ",Toast.LENGTH_LONG).show();

            }
        });


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNav);

        bottomNavigationView.setSelectedItemId(R.id.home);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.mapp:
                        startActivity(new Intent(getApplicationContext(), MapActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.veterinary:
                        startActivity(new Intent(getApplicationContext(), VeterinaryActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.calender:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }

                return false;
            }
        });

    }
}
