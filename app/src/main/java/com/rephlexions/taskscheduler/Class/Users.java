package com.rephlexions.taskscheduler.Class;

public class Users {
    private String email;
    private  String name;
    private  String date;
    private  String gender;
    private  String owner;
    private  String petinfo;
    private  String petname;

    public Users() {
    }

    public Users(String email, String name, String date, String gender, String owner, String petinfo, String petname) {
        this.email = email;
        this.name = name;
        this.date = date;
        this.gender = gender;
        this.owner = owner;
        this.petinfo = petinfo;
        this.petname = petname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPetinfo() {
        return petinfo;
    }

    public void setPetinfo(String petinfo) {
        this.petinfo = petinfo;
    }

    public String getPetname() {
        return petname;
    }

    public void setPetname(String petname) {
        this.petname = petname;
    }
}
