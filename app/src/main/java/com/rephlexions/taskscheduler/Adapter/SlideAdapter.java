package com.rephlexions.taskscheduler.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;



import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.rephlexions.taskscheduler.R;


public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SlideAdapter(Context context){

        this.context = context;
    }

    public int[] slide_images = {
            R.drawable.screen_1,
            R.drawable.screen_2,
            R.drawable.screen_3
    };

    public String[] slide_Big_title = {
        "Get Started!",
         "Don't forgot to Scan!",
          "Start Tracking!"
    };
    public String[] slide_title = {
            "Welcome to our app!",
            "Fill the boxes with \nyour information",
            "We're done!"
    };
    public String[] slide_description = {
                    "This guide will help you to learn how to usethe app, swipe right and follow instructions.",
            "Allow the app to access your camera and scan the QR CODE after you finish typing your info",
            "Enjoy our app and look after your pet."
    };


    @Override
    public int getCount() {
        return slide_Big_title.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object ;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater =(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView = view.findViewById(R.id.illus);
        TextView slideBigTitle = view.findViewById(R.id.Btitle);
        TextView slideTitle = view.findViewById(R.id.title);
        TextView slideDescription = view.findViewById(R.id.description);

        slideImageView.setImageResource(slide_images[position]);
        slideBigTitle.setText(slide_Big_title[position]);
        slideTitle.setText(slide_title[position]);
        slideDescription.setText(slide_description[position]);

        container.addView(view);


        return view;


    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
         container.removeView((RelativeLayout)object);
    }
}
