package com.rephlexions.taskscheduler.Class;

public class User {
    public String petname, email, name, owner, date, petinfo,gender, deviceid ;

    public User(String petname, String email,String name, String owner, String date, String petinfo, String gender,String deviceid) {
        this.petname = petname;
        this.email = email;
        this.name = name;
        this.owner = owner;
        this.date = date;
        this.petinfo = petinfo;
        this.gender = gender;
        this.deviceid = deviceid;
    }
}