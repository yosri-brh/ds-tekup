package com.rephlexions.taskscheduler.Activities;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.SphericalUtil;
import com.rephlexions.taskscheduler.Class.ServiceBg;
import com.rephlexions.taskscheduler.MainActivity;
import com.rephlexions.taskscheduler.R;


import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, RoutingListener {


    DatabaseReference loca;
    DatabaseReference Trackreference;
    Button Track;
    double distance,km;
    TextView distance_tv;

    //google map object
    private GoogleMap mMap;

    //current and destination location objects
    Location myLocation;
    Location destinationLocation=null;
    protected LatLng start=null;
    protected LatLng end=null;

    //to get location permissions.
    private final static int LOCATION_REQUEST_CODE = 23;
    boolean locationPermission=false;

    //polyline object
    private List<Polyline> polylines=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        distance_tv =findViewById(R.id.distance_tv);


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNav);
        bottomNavigationView.setSelectedItemId(R.id.mapp);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId())
                {
                    case R.id.mapp:
                        startActivity(new Intent(getApplicationContext(), MapActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.veterinary:
                        startActivity(new Intent(getApplicationContext(), VeterinaryActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.calender:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.account:
                        startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                }

                return false;
            }
        });

        Track = findViewById(R.id.Track);
        Track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(locationPermission) {
                        getMyLocation();
                        findPet();

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        //request location permission.
        requestPermision();

        //init google map fragment to show map.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_google);
        mapFragment.getMapAsync(this);
    }

    private void requestPermision()
    {
//        if(ActivityCompat.checkSelfPermission(MapActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
//        {
//            getMyLocation();
//            locationPermission=true;
//
//
//        }else{
//            ActivityCompat.requestPermissions(MapActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},44);
//        }

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST_CODE);
        }
        else{
            locationPermission=true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //if permission granted.
                    locationPermission=true;
                    getMyLocation();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    //to get user location
    private void getMyLocation(){
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                if(location != null){

                    myLocation=location;
                    LatLng ltlng=new LatLng(location.getLatitude(),location.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                            ltlng, 13f);
                    mMap.animateCamera(cameraUpdate);

                }


            }
        });

        //get destination location when user click on map


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);


            if(locationPermission) {
                getMyLocation();

            }

    }



    // function to find Routes.
    public void Findroutes(LatLng Start, LatLng End)
    {
        if(Start==null || End==null) {
            Toast.makeText(MapActivity.this,"Unable to get location", Toast.LENGTH_LONG).show();
        }
        else
        {

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(Start, End)
                    .key("AIzaSyCDlRjn2mcgBkMCMRHLSjXuXwZ6OM1ZIGo")  //also define your api key here.
                    .build();
            routing.execute();
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar= Snackbar.make(parentLayout, e.toString(), Snackbar.LENGTH_LONG);
        snackbar.show();
//        Findroutes(start,end);
    }

    @Override
    public void onRoutingStart() {
        Toast.makeText(MapActivity.this,"PeTrack finding you the shortest path ...",Toast.LENGTH_LONG).show();
    }

    //If Route finding success..
    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
        if(polylines!=null) {
            polylines.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        LatLng polylineStartLatLng=null;
        LatLng polylineEndLatLng=null;


        polylines = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i <route.size(); i++) {

            if(i==shortestRouteIndex)
            {
                polyOptions.color(getResources().getColor(R.color.colorPrimary));
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                polylineStartLatLng=polyline.getPoints().get(0);
                int k=polyline.getPoints().size();
                polylineEndLatLng=polyline.getPoints().get(k-1);
                polylines.add(polyline);

            }
            else {

            }

        }

        //Add Marker on route starting position
//        Drawable circleDrawable = getResources().getDrawable(R.drawable.dot_maps);
//        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
        int height = 60;
        int width = 60;
        BitmapDrawable bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.mipmap.dot_yellow_foreground);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap sMarker = Bitmap.createScaledBitmap(b, width, height, false);




        MarkerOptions startMarker = new MarkerOptions();
        startMarker.position(polylineStartLatLng);
        startMarker.title("My Location");
        startMarker.icon(BitmapDescriptorFactory.fromBitmap(sMarker));
        mMap.addMarker(startMarker);

        int height2 = 70;
        int width2 = 70;

        BitmapDrawable bitmapdraww = (BitmapDrawable)getResources().getDrawable(R.mipmap.pinmaps_yellow_foreground);
        Bitmap bw = bitmapdraww.getBitmap();
        Bitmap eMarker = Bitmap.createScaledBitmap(bw, width2, height2, false);

        //Add Marker on route ending position
        MarkerOptions endMarker = new MarkerOptions();
        endMarker.position(polylineEndLatLng);
        endMarker.title("Your pet is here");
        endMarker.icon(BitmapDescriptorFactory.fromBitmap(eMarker));
        mMap.addMarker(endMarker);
    }
//    private BitmapDescriptor bitmapDescriptor(Context context,int vectorResId){
//        Drawable vectorDarawable = ContextCompat.getDrawable(context,vectorResId);
//        vectorDarawable.setBounds(0,0,vectorDarawable.getIntrinsicWidth(),
//                vectorDarawable.getIntrinsicHeight());
//        Bitmap bitmap = Bitmap.createBitmap(vectorDarawable.getIntrinsicWidth(),
//                vectorDarawable.getIntrinsicHeight(),Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        return BitmapDescriptorFactory.fromBitmap(bitmap);
//    }

    @Override
    public void onRoutingCancelled() {
        Findroutes(start,end);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Findroutes(start,end);

    }
    public void findPet(){

        loca = FirebaseDatabase.getInstance().getReference("Location");
        Trackreference = FirebaseDatabase.getInstance().getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        Trackreference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String ID = dataSnapshot.child("deviceid").getValue(String.class);

                loca.child(ID).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String Lat = dataSnapshot.child("latitude").getValue(String.class);
                        String Lng = dataSnapshot.child("longitude").getValue(String.class);
                        String time = dataSnapshot.child("time").getValue(String.class);



                        LatLng latLng = new LatLng(Double.parseDouble(Lat),Double.parseDouble(Lng));
//                            MarkerOptions options = new MarkerOptions().position(latLng).title("Your Pet Is here at "+time);
//                        googleMap.clear();
//                            googleMap.addMarker(options);
                        end=latLng;
                        mMap.clear();

                        start=new LatLng(myLocation.getLatitude(),myLocation.getLongitude());
                        //start route finding
                        mMap.clear();

                        distance = SphericalUtil.computeDistanceBetween(start,end);
                        km = distance/1000;
                        DecimalFormat d = new DecimalFormat("##.##");
                        distance_tv.setText(d.format(distance/1000)+ " km");
                        startService();


//                        BigDecimal number = new BigDecimal(distance);
//                        number.round(MathContext.DECIMAL64);





//                        Track.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Findroutes(start,end);
//                            }
//                        });

                        Findroutes(start,end);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void startService() {

        Intent serviceIntent = new Intent(this, ServiceBg.class);
        DecimalFormat d = new DecimalFormat("##.##");
        String Var = d.format(km);
        serviceIntent.putExtra("inputExtra", Var);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

}