package com.rephlexions.taskscheduler.AuthAndRegister;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.rephlexions.taskscheduler.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    ProgressBar Reset_Password_progress;
    EditText login_mail;
    Button Reset_Password_Btn,Return_To_Login;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        login_mail = findViewById(R.id.login_mail);
        Reset_Password_progress = findViewById(R.id.Reset_Password_progress);
        Reset_Password_Btn =findViewById(R.id.Reset_Password_Btn);
        Return_To_Login =findViewById(R.id.Return_To_Login);
        Reset_Password_progress.setVisibility(View.INVISIBLE);


        firebaseAuth =FirebaseAuth.getInstance();

        Return_To_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });

        Reset_Password_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Reset_Password_progress.setVisibility(View.VISIBLE);
                Reset_Password_Btn.setVisibility(View.INVISIBLE);

                firebaseAuth.sendPasswordResetEmail(login_mail.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Toast.makeText(ForgotPasswordActivity.this, "Your Reset Link Has been Sent To Your Email", Toast.LENGTH_SHORT).show();
                        Reset_Password_progress.setVisibility(View.INVISIBLE);
                        Reset_Password_Btn.setVisibility(View.VISIBLE);
                    }
                });

            }
        });

    }
}
