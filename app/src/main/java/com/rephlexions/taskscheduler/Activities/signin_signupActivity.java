package com.rephlexions.taskscheduler.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.rephlexions.taskscheduler.AuthAndRegister.ForgotPasswordActivity;
import com.rephlexions.taskscheduler.AuthAndRegister.LoginActivity;
import com.rephlexions.taskscheduler.AuthAndRegister.RegisterActivity;
import com.rephlexions.taskscheduler.R;

public class signin_signupActivity extends AppCompatActivity {

    Button loginBtn,goregBtn;
    private FirebaseAuth mAuth;
    TextView ForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_signup);

        loginBtn = findViewById(R.id.loginBtn);
        goregBtn = findViewById(R.id.goregBtn);
        mAuth = FirebaseAuth.getInstance();
        ForgotPassword = findViewById(R.id.ForgotPassword);

        ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginActivity = new Intent(getApplicationContext(),ForgotPasswordActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginActivity);
                finish();

            }
        });
        goregBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent registerActivity = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(registerActivity);
                finish();

            }
        });

    }
    private void UpdateUI() {

        Intent homeActivity = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(homeActivity);
        finish();

    }
    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null )
        {
            UpdateUI();
        }
    }
}
