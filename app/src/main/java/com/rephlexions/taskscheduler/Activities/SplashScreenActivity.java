package com.rephlexions.taskscheduler.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.rephlexions.taskscheduler.R;

public class SplashScreenActivity extends AppCompatActivity {

    ImageView logo ;
    int SPLASH_DELAY=2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setBackgroundDrawable(null);
        initiazeView();
        animateLogo();
        updateUI();
    }

    private void initiazeView() {

        logo  = findViewById(R.id.logo);

    }
    private void animateLogo() {

        Animation fadingInAnimation = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        fadingInAnimation.setDuration(SPLASH_DELAY);
        logo.startAnimation(fadingInAnimation);
    }
    private void updateUI() {

        new Handler().postDelayed(()-> {
            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
            finish();
        }, SPLASH_DELAY);
        }



}
