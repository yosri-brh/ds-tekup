package com.rephlexions.taskscheduler.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;


import com.google.firebase.auth.FirebaseAuth;
import com.rephlexions.taskscheduler.AuthAndRegister.LoginActivity;
import com.rephlexions.taskscheduler.Class.ServiceBg;
import com.rephlexions.taskscheduler.R;

public class SettingActivity extends AppCompatActivity {

    ImageView back,LanArrow,LogOut;
    Switch switch1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        back = findViewById(R.id.back);
        LogOut = findViewById(R.id.LogOut);
        LanArrow = findViewById(R.id.LangArrow);
        switch1 = findViewById(R.id.switch1);
        if(switch1.isChecked()){
            Intent serviceIntent = new Intent(this, ServiceBg.class);
            stopService(serviceIntent);
        }
        LanArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent language_Activity = new Intent(getApplicationContext(), LanguageActivity.class);
                startActivity(language_Activity);
                finish();

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent account_Activity = new Intent(getApplicationContext(), AccountActivity.class);
                startActivity(account_Activity);
                finish();
            }
        });
        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent loginactivity = new Intent(getApplicationContext(), LoginActivity.class);
                loginactivity.putExtra("finish", true);
                loginactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginactivity);
                finish();
            }
        });









    }
}
