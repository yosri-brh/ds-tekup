package com.rephlexions.taskscheduler.Activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rephlexions.taskscheduler.Class.Upload;
import com.rephlexions.taskscheduler.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class Upload_Gallery extends AppCompatActivity {

    private ImageView Upic,BackArrow;
    private Button BtnU,BtnC;
    private EditText mEditeTextFileName;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;






    private final int IMG_REQUEST_ID = 10;
    private Uri  IMGUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload);



        Upic = findViewById(R.id.image_view);
        BackArrow = findViewById(R.id.back);
        BtnC = findViewById(R.id.button_choose_image);
        BtnU = findViewById(R.id.button_upload);
        mEditeTextFileName = findViewById(R.id.edit_text_file_name);
        BtnU.setEnabled(false);

        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent accountActivity = new Intent(getApplicationContext(), AccountActivity.class);
                startActivity(accountActivity);
                finish();
            }
        });

        BtnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestImage();
            }
        });
        BtnU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInFirebase();
            }
        });




    }

    private void requestImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),IMG_REQUEST_ID );


    }
    private String getFileExtension(Uri uri){

        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));

    }
    private void saveInFirebase(){

        if (IMGUri != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Hold on... Just wait :p");
            progressDialog.show();

            final StorageReference fileRefrence = mStorageRef.child(System.currentTimeMillis()
                    +"."+getFileExtension(IMGUri));

            fileRefrence.putFile(IMGUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if(task.isSuccessful()){
                        fileRefrence.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                progressDialog.dismiss();
                                Toast.makeText(Upload_Gallery.this, "Done Tadaa :D",Toast.LENGTH_LONG).show();
                                Upload upload = new Upload(mEditeTextFileName.getText().toString().trim()
                                        ,uri.toString());
                                String uploadId = mDatabaseRef.push().getKey();
                                mDatabaseRef.child(uploadId).setValue(upload);
                                Intent accountActivity = new Intent(getApplicationContext(), AccountActivity.class);
                                startActivity(accountActivity);
                                finish();


                            }
                        });
                    }

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double Progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Looks Good right ? :D " + (int) Progress + "%" );

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(Upload_Gallery.this, "OMG check please :'( " + e.getMessage()
                            ,Toast.LENGTH_SHORT).show();

                }
            });




        }else{
            Toast.makeText(this,"No file Selected :(",Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMG_REQUEST_ID && resultCode == RESULT_OK && data != null && data.getData() != null){

            IMGUri =  data.getData();
            try {
                Bitmap bitmapImg = MediaStore.Images.Media.getBitmap(getContentResolver(),IMGUri);
                Upic.setImageBitmap(bitmapImg);
                BtnU.setEnabled(true);
                BtnC.setEnabled(false);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
